
/*
 Created by: Wesley Vanbrabant, Lorenz De Cuypere, Maxim Vanhove
 Date: 26-11-2014
 Name: app.js
 Description: HaveACookie, To-do Application
 */

//Alle date die we gaan opslaan in localstorage
var AppData = {
    "information":{
        "title":"HaveACookie, To-do Application",
        "version":"1.0",
        "modified":"01-12-2014",
        "authors":"Wesley Vanbrabant, Lorenz De Cuypere, Maxim Vanhove"
    },
    "lists":[],
    "animation": true,
    "settings":{
        sortedItem: 'created'
    }
};

//Enumeration of priorities
var Priorities = {
    LOW:1,
    NORMAL:2,
    MEDIUM:3,
    HIGH:4,
    properties:{
        1:{id:1, name:'Low'},
        2:{id:2, name:'Normal'},
        3:{id:3, name:'Medium'},
        4:{id:4, name:'High'}
    }
};


//Als het html document geladen is
(function(){

    var history = [];
    var historyindex = 1;

    function historyUpdate(){
        var data = JSON.stringify(AppData);
        history.splice((historyindex+1), 1000);
        history.push(data);
        historyindex = history.length-1;
    }

    function historyUndo(){
        if(historyindex > 0) {
            historyindex--;
            var i = (historyindex);
            if (i >= 0)AppData = JSON.parse(history[i]);
            renderUI();
        }
    }

    function historyRedo(){
        if(historyindex + 1 < history.length) {
            historyindex++;
            var i = (historyindex);
            if (i >= 0)AppData = JSON.parse(history[i]);
            renderUI();
        }
    }

    $('#undo').on('click', function(){
        historyUndo();
    });

    $('#redo').on('click', function(){
        historyRedo();
    });

    //Event handler: Toevoegen van een taak in het inputfield
    var inputTask = $('.inputTask');
    inputTask.keypress(function( event ) {
        if ( event.which == 13 ) {
            var list = document.querySelector('.page.active');
            if(list) {
                var task = createTask(this.value);
                var i = getIndexByName(AppData.lists, list.id);
                if (i)AppData.lists[i].tasks.push(task); //Voegt taak toe in de lijst
                updateAll(); //UI word gereset
                this.value = '';

                var el = $(this);
                $('.item').each(function () {
                    if ($(this).attr('data-id') == task.id) {
                        el = $(this);
                    }
                });
                scrollTo(el)
            }
            else{
                if(AppData.lists.length > 0){alert('Please select a category in the navigation')}
                else{alert('Please create a category by clicking the plus in the navigation')}
            }
        }
    });

    $('.inputCat').keypress(function( event ) {
        if ( event.which == 13 ) {
            var list = document.querySelector('.page.active');
            if(list) {
                var cat = newCategory(this.value);
                var i = getIndexByName(AppData.lists, list.id);
                var id;
                if (i) {
                    AppData.lists[i].categories.push(cat)
                }
                updateAll(); //UI word gereset
                this.value = '';
            }
            else{
                if(AppData.lists.length > 0){alert('Please select a category in the navigation')}
                else{alert('Please create a category by clicking the plus in the navigation')}
            }
        }
    });

    $('body').keypress(function( event ) {
            /* 26 = ctrl + Z */
            if ( event.which == 26) {
                if(cmdDown = true)historyUndo();
            }
            /* 25 = ctrl + Y */
            if ( event.which == 25) {
                if(cmdDown = true)historyRedo();
            }
        });

    $('.sortby').on('click', function(ev){
        ev.preventDefault();
        switch (this.innerText){
            default:
                AppData.settings.sortedItem = 'created';
                break;
            case 'priority':
                AppData.settings.sortedItem = 'priority';
                break;
            case 'alphabet':
                AppData.settings.sortedItem = 'content';
                break;
            case 'due-date':
                AppData.settings.sortedItem = 'duedate';
                break;
            case 'completed':
                AppData.settings.sortedItem = 'completed';
                break;
        }
        updateAll();
    });

    //Function: Maakt een nieuwe task met aangeleverde content
    function createTask(content, category){
        //Task object
        var task = new Object();
        task.id = Utils.guid();
        task.completed = false;
        task.content = content;
        task.category = category || null;
        task.priority = Priorities.LOW;
        task.created = new Date();
        task.duedate = '';
        task.description = '';
        task.collapsed = false;
        task.reminder = {
            amount: 0,
            timestamp: 'uur',
            reminded: true
        };
        return task;
    }

    //Function: Maakt een nieuwe lijst
    function newList(name){
        var list = new Object();
        list.id = Utils.guid();
        list.name = Utils.spaceTo_(name); //Spaties worden vervangen door _ om te kunnen gebruiken in id's en classes
        list.tasks = [];
        list.categories = [];
        return list;
    }

    function newCategory(name){
        var category = new Object();
        category.id = Utils.guid();
        category.name = Utils.spaceTo_(name); //Spaties worden vervangen door _ om te kunnen gebruiken in id's en classes
        category.tasks = [];
        return category;
    }

    function getIndexByName(list, name){
        for(var i in list){
            if(list[i].name == name) return i;
        }
    }

    function getIndexCatById(id, list){
        for(var i in list.categories){
            if(list.categories[i].id == id) return i;
        }
    }

    function removeTaskById(id){
        for(list in AppData.lists){
            var index = _.findIndex(AppData.lists[list].tasks,function(task){
                return task.id == id;
            });

            if(index > -1){
                //Remove the task from the Array
                AppData.lists[list].tasks.splice(index, 1);
                //Update the User Interface --> to call
                updateAll();
            }

            for(var cat in AppData.lists[list].categories){
                var indexx = _.findIndex(AppData.lists[list].categories[cat].tasks,function(task){
                    return task.id == id;
                });

                if(indexx > -1){
                    //Remove the task from the Array
                    AppData.lists[list].categories[cat].tasks.splice(indexx, 1);
                    //Update the User Interface --> to call
                    updateAll();
                }
            }
        }
    }

    function removeListById(id){
        var index = -1;
        for(var i in AppData.lists){
            if(AppData.lists[i].id == id)index=i;
        }

        if(index > -1){
            //Remove the list from the Array
            AppData.lists.splice(index, 1);
            //Update the User Interface --> to call
            updateAll();
        }
    }

    function removeCategoryById(id){
        for(var i in AppData.lists){
            for(var x in AppData.lists[i].categories)
                if (AppData.lists[i].categories[x].id == id){
                    AppData.lists[i].categories.splice(x, 1);
                }
        }

        updateAll();
    }

    function setContentById(id, content){
        for(list in AppData.lists){
            var index = _.findIndex(AppData.lists[list].tasks,function(task){
                return task.id == id;
            });

            if(index > -1){
                AppData.lists[list].tasks[index].content = content;
                updateAll();
            }

            for(var cat in AppData.lists[list].categories){
                var indexx = _.findIndex(AppData.lists[list].categories[cat].tasks,function(task){
                    return task.id == id;
                });

                if(indexx > -1){
                    AppData.lists[list].categories[cat].tasks[indexx].content = content;
                    updateAll();
                }
            }
        }
    }

    function setPriorityById(id, priority){
        for(list in AppData.lists){
            var index = _.findIndex(AppData.lists[list].tasks,function(task){
                return task.id == id;
            });

            if(index > -1){
                switch (priority){
                    default:
                        AppData.lists[list].tasks[index].priority = Priorities.LOW;
                        break;
                    case "normal":
                        AppData.lists[list].tasks[index].priority = Priorities.NORMAL;
                        break;
                    case "medium":
                        AppData.lists[list].tasks[index].priority = Priorities.MEDIUM;
                        break;
                    case "high":
                        AppData.lists[list].tasks[index].priority = Priorities.HIGH;
                        break;
                }
            }

            for(var cat in AppData.lists[list].categories){
                var indexx = _.findIndex(AppData.lists[list].categories[cat].tasks,function(task){
                    return task.id == id;
                });

                if(indexx > -1){
                    switch (priority){
                        default:
                            AppData.lists[list].categories[cat].tasks[indexx].priority = Priorities.LOW;
                            break;
                        case "normal":
                            AppData.lists[list].categories[cat].tasks[indexx].priority = Priorities.NORMAL;
                            break;
                        case "medium":
                            AppData.lists[list].categories[cat].tasks[indexx].priority = Priorities.MEDIUM;
                            break;
                        case "high":
                            AppData.lists[list].categories[cat].tasks[indexx].priority = Priorities.HIGH;
                            break;
                    }
                }
            }
        }
    }

    function setDueDateById(id, date){
        for(list in AppData.lists){
            var index = _.findIndex(AppData.lists[list].tasks,function(task){
                return task.id == id;
            });

            if(index > -1){
                AppData.lists[list].tasks[index].duedate = date;
            }

            for(var cat in AppData.lists[list].categories){
                var indexx = _.findIndex(AppData.lists[list].categories[cat].tasks,function(task){
                    return task.id == id;
                });

                if(indexx > -1){
                    AppData.lists[list].categories[cat].tasks[indexx].duedate = date;
                }
            }
        }
    }

    function setReminderById(id, amount, timestamp){
        for(list in AppData.lists){
            var index = _.findIndex(AppData.lists[list].tasks,function(task){
                return task.id == id;
            });

            if(index > -1){
                AppData.lists[list].tasks[index].reminder.amount = amount;
                AppData.lists[list].tasks[index].reminder.timestamp = timestamp;
                AppData.lists[list].tasks[index].reminder.reminded = false;
            }

            for(var cat in AppData.lists[list].categories){
                var indexx = _.findIndex(AppData.lists[list].categories[cat].tasks,function(task){
                    return task.id == id;
                });

                if(indexx > -1){
                    AppData.lists[list].categories[cat].tasks[indexx].reminder.amount = amount;
                    AppData.lists[list].categories[cat].tasks[indexx].reminder.timestamp = timestamp;
                    AppData.lists[list].categories[cat].tasks[indexx].reminder.reminded = false;
                }
            }
        }
    }

    function toggleCompletedById(id){
        for(list in AppData.lists){
            var index = _.findIndex(AppData.lists[list].tasks,function(task){
                return task.id == id;
            });

            if(index > -1){
                AppData.lists[list].tasks[index].completed = !AppData.lists[list].tasks[index].completed;
                if(AppData.lists[list].tasks[index].completed){
                    if(AppData.animation){
                        AppData.animation = false;
                        document.getElementById('audiotag1').play();
                        showAnimation();
                    }
                }
            }

            for(var cat in AppData.lists[list].categories){
                var indexx = _.findIndex(AppData.lists[list].categories[cat].tasks,function(task){
                    return task.id == id;
                });

                if(indexx > -1){
                    AppData.lists[list].categories[cat].tasks[indexx].completed = !AppData.lists[list].categories[cat].tasks[indexx].completed;
                    if(AppData.lists[list].categories[cat].tasks[indexx].completed){
                        if(AppData.animation){
                            AppData.animation = false;
                            document.getElementById('audiotag1').play();
                            showAnimation();
                        }
                    }
                }
            }
        }
    }

    function toggleCollapsedById(id){
        for(list in AppData.lists){
            var index = _.findIndex(AppData.lists[list].tasks,function(task){
                return task.id == id;
            });

            if(index > -1){
                AppData.lists[list].tasks[index].collapsed = !AppData.lists[list].tasks[index].collapsed;
            }

            for(var cat in AppData.lists[list].categories){
                var indexx = _.findIndex(AppData.lists[list].categories[cat].tasks,function(task){
                    return task.id == id;
                });

                if(indexx > -1){
                    AppData.lists[list].categories[cat].tasks[indexx].collapsed = !AppData.lists[list].categories[cat].tasks[indexx].collapsed;
                }
            }
        }
    }

    function toggleCategoryCompetedById(id){
        for(var i in AppData.lists){
            for(var x in AppData.lists[i].categories)
            if (AppData.lists[i].categories[x].id == id){
                for(y in AppData.lists[i].categories[x].tasks){
                    AppData.lists[i].categories[x].tasks[y].completed = true;
                }
            }
        }

        updateAll();
    }

    function showAnimation(){
        var delay;
        for(var n = 1; n<10; n++) {
            delay = 250*(n);
            showFrame(n, delay);
        }
    }

    function showFrame(n, delay){
        setTimeout(function(){
            $('#logo').attr('src', 'content/images/gif/logo' + n + '.png');
            if(n == 9)AppData.animation = true;
        }, delay);
    }

    function getPriorityClass(priority){
        switch (priority){
            default:
                return "low";
                break;
            case 2:
                return "normal";
                break;
            case 3:
                return "medium";
                break;
            case 4:
                return "high";
                break;
        }
    }

    //Function: Get HTML for a Task
    function getHTMLForTask(task){
        var htmlContent = '';
        var low =  (task.priority == 1 ? ' selected="selected"':'');
        var normal =  (task.priority == 2 ? ' selected="selected"':'');
        var medium =  (task.priority == 3 ? ' selected="selected"':'');
        var high =  (task.priority == 4 ? ' selected="selected"':'');
        var uur = (task.reminder.timestamp == 'uur' ? ' selected="selected"':'');
        var dag = (task.reminder.timestamp == 'dag' ? ' selected="selected"':'');
        var week = (task.reminder.timestamp == 'week' ? ' selected="selected"':'');
        var date = new Date(task.duedate);
        var due = '';
        var now = new Date();
        if (date < now){
            due = ' due'
        }
        date = (date == 'Invalid Date' ? '':date.getDate() + '/' + (date.getMonth()+1) + '/' + date.getFullYear());
        var complete = (task.completed ? ' item-completed':'');
        var iconClass = (task.completed ? 'fa fa-check-circle':'fa fa-circle-o');
        var collapsed = (task.collapsed ? ' style="display: block;"':'');
        if(task != null){
            htmlContent += ''
            + '<ul class="item" data-id="' + task.id + '">'
            + '<section class="task-content '+ getPriorityClass(task.priority) + complete +'">'
            + '<section class="row">'
            + '<section class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xxs-2">'
            + '<a href="#" class="task-complete" title="">'
            + '<i class="'+ iconClass +'"></i>'
            + '</a>'
            + '</section>'
            + '<section class="content col col-xl-10 col-lg-10 col-md-10 col-sm-10 col-xs-10 col-xxs-9">'
            + task.content
            + '</section>'
            + '<section class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xxs-1">'
            + '<a href="#" class="task-remove" title="">'
            + '<i class="fa fa-close"></i>'
            + '</a>'
            + '</section>'
            + '</section>'
            + '<section class="row row-due-date">'
            + '<section class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xxs-2"><p> </p></section>'
            + '<section class="col col-xl-10 col-lg-10 col-md-10 col-xs-10 col-xxs-10 due-date'+ due +'"> ' + date + ' </section>'
            + '<section class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-1 col-xxs-1"></section>'
            + '</section>'
            + '</section>'
            + '<section class="row rowTaskItems" '+collapsed+'>'
            + '<section class="hiddenMenuItem"> Priority: <select class="inputInfo inputPriority">'
            + '<option value="low"' + low + '>Low</option>'
            + '<option value="normal"' + normal + '>Normal</option>'
            + '<option value="medium"' + medium + '>Medium</option>'
            + '<option value="high"' + high + '>High</option>'
            + '</select></section>'
            + '<section class="hiddenMenuItem">Due-date: <input type="text" placeholder="dd/mm/yyyy" value="'+date+'" class="inputInfo inputDueDate"></section>'
            + '<section class="hiddenMenuItem">Reminder: <input type="number" name="reminder" class="inputInfo inputReminder" value="'+task.reminder.amount+'">'
            + '<select class="inputInfo inputTimestamp">'
            + '<option value="uur" ' + uur + '>hours</option>'
            + '<option value="dag" ' + dag + '>days</option>'
            + '<option value="week" ' + week + '>weeks</option>'
            + '</select></section>'
            + '</ul>';
        }
        return htmlContent;
    }

    //Function: Get HTML for Navigation
    function getHTMLForNav(list){
        var htmlContent = '';
        htmlContent += ''
        + '<li class="navitem '+list.name+'">'
        + '<a href="#/'+ list.name +'" class="'+ list.name +'">'
        + Utils._ToSpace(list.name)
        + '</a>'
        + '<a href="#" class="list-remove" data-id="' + list.id + '" title="">'
        + '<i class="fa fa-close menu-cross"></i>'
        + '</a>'
        +'</li>';
        return htmlContent;
    }

    function getHTMLForCat(category){
        var htmlContent = '';
        var name = Utils._ToSpace(category.name);

        //Render all tasks
        var tasksHTML = '';
        for (task in category.tasks){
            tasksHTML += getHTMLForTask(category.tasks[task])
        }

        htmlContent += ''
            + '<section class="category" data-id="'+ category.id +'">'
            + '<section class="category-header row">'
            + '<section class="col col-xl-2 col-lg-2 col-md-2 col-sm-2 col-xs-2 col-xxs-2"><a href="#"><i class="fa fa-circle-o"></i></a></section>'
            + '<section class="col col-xs-5 col-lg-5 col-md-5 col-sm-5 col-xs-5 col-xxs-5"> '+ name + '</section>'
            + '<section class="col col-xl-1 col-lg-1 col-md-1 col-sm-1 col-xs-2 col-xxs-2"><a href="#"><i class="fa fa-close unskew"></i></a></section>'
            + '</section>'
            + '<section class="category-tasks row">'
            + '<ul>'
            + '<li>'
            + tasksHTML
            + '</li>'
            + '</ul>'
            + '</section>'
            + '<input class="input inputTaskCat" placeholder="Add a task to your list here...">'
            + '</section>';


        return htmlContent;
    }

    function getHTMLForList(list){

        //Render tasks
        var tasksHTML = '';
        for (task in list.tasks){
            tasksHTML += getHTMLForTask(list.tasks[task])
        }

        //Render cats
        var catsHTML = '';
        for (var i in list.categories){
            catsHTML += getHTMLForCat(list.categories[i])
        }

        //Render list with tasks and categories
        var listHTML = '';
        listHTML += ''
        + '<div id="' + list.name + '" class="page" data-id="' + list.id + '">'
        + '<div class="cats-container">'
        + catsHTML
        + '</div>'
        + '<div class="tasks-container">'
        + tasksHTML
        +'</div>';

        return listHTML;
    }

    function addNewList(name){
        var list = newList(name);
        AppData.lists.push(list);
        //AppData.activeList = list.name;
        updateAll();
        window.location.href = '#/' + list.name;
    }

    function listsContains(name){
        for (i in AppData.lists){
            if (AppData.lists[i].name == name)return true;
        }
        return false;
    }

    function sortTasks(){
        for(i in AppData.lists) {
            //Sorting the Array and reverse
            switch (AppData.settings.sortedItem) {
                default :
                    AppData.lists[i].tasks = _.sortBy(AppData.lists[i].tasks, 'created');
                    break;
                case 'priority':
                    AppData.lists[i].tasks = _.sortBy(AppData.lists[i].tasks, 'priority').reverse();
                    break;
                case 'content':
                    AppData.settings.sortedItem = 'content';
                    AppData.lists[i].tasks = _.sortBy(AppData.lists[i].tasks, 'content');
                    break;
                case 'duedate':
                    AppData.settings.sortedItem = 'duedate';
                    AppData.lists[i].tasks = _.sortBy(AppData.lists[i].tasks, 'duedate');
                    break;
                case 'completed':
                    AppData.settings.sortedItem = 'completed';
                    AppData.lists[i].tasks = _.sortBy(AppData.lists[i].tasks, 'completed').reverse();
                    break;
            }
        }
    }

    function renderNavigation(){
        /*render navigation*/
        var navContainer = document.querySelector('.nav-container');
        navContainer.innerHTML = '';
        var lists = AppData.lists;
        for (var list in lists){
            navContainer.innerHTML += getHTMLForNav(lists[list]);
        }

        // add the + button
        navContainer.innerHTML += '<li ><a href="#" class="list-add" ><i class="fa fa-plus"></i></a></li>';
    }

    function renderLists(){
        /* render lists */
        var lists = AppData.lists;
        var pagesContainer = document.querySelector('.pages-container');
        pagesContainer.innerHTML = '';
        for (var list in lists){
            pagesContainer.innerHTML += getHTMLForList(lists[list]);
        }
    }

    function registerEventHandlers(){
        /* Event Handlers */
        $('.task-remove').on('click', function(ev){
            ev.preventDefault();
            var id = $(this).closest('.item').attr('data-id');
            removeTaskById(id);
        });

        $('.list-add').on('click',function(ev){
            ev.preventDefault();
            var list = newList('NewList');
            var html = getHTMLForNav(list);
            $(this).parent().before(html);
            $(this).parent().parent().find('.NewList').children().first().attr('contenteditable', 'true').focus();
            $(this).parent().parent().find('.NewList').children().first().text('');
            $(this).parent().parent().find('.NewList').select();
            $('.NewList').children().first().keypress(function( event ) {
                if ( event.which == 13 ) {
                    event.preventDefault();
                    this.blur();
                }
            }).on('blur', function(){
                if(/^[a-zA-Z0-9\ ]+$/.test(this.innerText)) {
                    addNewList(this.innerText);
                }
                else{
                    alert('The name can only contain letters and numbers');
                    updateAll();
                }
            });
        });

        $('.navitem').on('click', function(ev){
            var href = $(this).children().first().attr('class');
            if(ev.target.classList[0] == 'navitem')window.location.href = '#/' + href;
        });

        $('.list-remove').on('click', function(ev){
            ev.preventDefault();
            if(confirm('Are you sure you want to delete this list?')){
                var id = $(this).attr('data-id');
                removeListById(id);
                var list;
                for(var i in AppData.lists){
                    list = AppData.lists[i].name;
                    window.location.href = '#/' + list;
                    break;
                }
            }
        });

        $('.inputInfo.inputPriority').on('change', function(ev){
            var id = $(this).closest('.item').attr('data-id');
            setPriorityById(id, this.value);
            updateAll();
        });

        $('.inputInfo.inputDueDate').keypress(function( event ) {
            if ( event.which == 13 ) {
                event.preventDefault();
                this.focus();
                this.blur();
            }
        })
            .on('change', function(){
                var d = new Date(this.value);
                if(d == 'Invalid Date'){
                    this.value = '';
                    updateAll();
                }
                else{
                    var id = $(this).closest('.item').attr('data-id');
                    setDueDateById(id, d);
                    updateAll();
                }
            })
            .on('blur', function(){
                var d = new Date(this.value);
                if(d == 'Invalid Date'){
                    this.value = '';
                }
                else{
                    var id = $(this).closest('.item').attr('data-id');
                    setDueDateById(id, d);
                    updateAll();
                }
            });

        $('.task-content').on('click', function(){
            $(this).parent().find('.rowTaskItems').slideToggle('fast', 'swing');
            var id = $(this).closest('.item').attr('data-id');
            toggleCollapsedById(id);
        });

        $('.task-complete').on('click', function(ev){
            ev.preventDefault();
            var id = $(this).closest('.item').attr('data-id');
            toggleCompletedById(id);
            updateAll();
        });

        $('.inputInfo.inputReminder').on('change', function(){
            var id = $(this).closest('.item').attr('data-id');
            var timestamp = $(this).parent().find('select').val();
            setReminderById(id, this.value, timestamp);
            checkReminders();
        });

        $('.inputInfo.inputTimestamp').on('change', function(){
            var id = $(this).closest('.item').attr('data-id');
            var amount =  $(this).parent().find('.inputReminder').val();
            setReminderById(id, amount, this.value);
        });

        $('.content').on('click', function(){
            $(this).attr('contenteditable', 'true').focus();
        })
            .on('blur', function(){
                $(this).attr('contenteditable', 'false');
                var id = $(this).closest('.item').attr('data-id');
                setContentById(id, this.innerText);
            })
            .keypress(function( event ) {
                if ( event.which == 13 ) {
                    event.preventDefault();
                    this.blur();
                }
            });

        $('.inputTaskCat').keypress(function( event ) {
            if ( event.which == 13 ) {
                var list = document.querySelector('.page.active');
                var task = createTask(this.value);
                var id = $(this).closest('.category').attr('data-id');
                var i = getIndexByName(AppData.lists, list.id);
                var ic = getIndexCatById(id, AppData.lists[i]);
                if (i){
                    AppData.lists[i].categories[ic].tasks.push(task);
                }
                updateAll(); //UI word gereset
                this.value = '';
            }
        });

        $( ".inputDueDate" ).datepicker();

        $('.category-header .fa-circle-o').on('click', function(ev){
            ev.preventDefault();
            var id = $(this).closest('.category').attr('data-id');
            toggleCategoryCompetedById(id);
        });

        $('.category-header .fa-close').on('click', function(ev){
            ev.preventDefault();
            var id = $(this).closest('.category').attr('data-id');
            removeCategoryById(id);
        });
    }

    function renderUI(){
        sortTasks();
        renderNavigation();
        renderLists();

        //Maak de huidige lijst terug actief
        if (listsContains(AppData.activeList)){
            document.querySelector('#' + AppData.activeList).classList.add('active');
            document.querySelector('.navitem.' + AppData.activeList).classList.add('active');
        }

        registerEventHandlers();
        checkReminders();
        Utils.store('HaveACookie', AppData);
    }

    function updateAll(){
        renderUI();
        historyUpdate();
    }

    function checkReminders(){
        var now = new Date();
        for(var i in AppData.lists){
            for(t in AppData.lists[i].tasks){
                if (!AppData.lists[i].tasks[t].reminder.reminded && AppData.lists[i].tasks[t].duedate != ''){
                    var dueDate = new Date(AppData.lists[i].tasks[t].duedate);
                    var MS;
                    switch (AppData.lists[i].tasks[t].reminder.timestamp){
                        case 'uur':
                            MS = 1000*60*60;
                            break;
                        case 'dag':
                            MS = 1000*60*60*24;
                            break;
                        case 'week':
                            MS = 1000*60*60*24*7;
                            break;
                    }
                    var remindDate = new Date(dueDate - AppData.lists[i].tasks[t].reminder.amount * MS);
                    if(remindDate < now){
                        AppData.lists[i].tasks[t].reminder.reminded = true;
                        alert("Reminder for: " + AppData.lists[i].tasks[t].content);
                    }
                }
            }
        }
    }

    function isElementInViewport(el) {

        if (typeof jQuery === "function" && el instanceof jQuery) {
            el = el[0];
        }

        var rect = el.getBoundingClientRect();

        return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
        );
    }

    function scrollTo(el){
        if(!isElementInViewport(el)){
            $('html, body').animate({
                scrollTop: el.offset().top
            }, 400);
        }
    }

    function applyDefaultAppValues(){
        AppData.activeList = 'Welcome';
        var listWelcome = newList('Welcome');
        var cat1 = newCategory('Settings');
        var task1 = createTask('Start using HaveACookie');
        var task2 = createTask('Create a task');
        var task3 = createTask('Select a task to edit the settings');
        var task4 = createTask('Sort tasks by priority in the settings');
        task1.completed = true;
        task2.priority = Priorities.NORMAL;
        task3.priority = Priorities.MEDIUM;
        task4.priority = Priorities.HIGH;
        listWelcome.tasks.push(task1);
        listWelcome.tasks.push(task2);
        listWelcome.tasks.push(task3);
        cat1.tasks.push(task4);
        listWelcome.categories.push(cat1);
        AppData.lists.push(listWelcome);
        AppData.lists.push(newList('Shopping list'));
        AppData.lists.push(newList('Ideas'));

        Utils.store('HaveACookie', AppData);
    }

    //Get the AppData from the datastorage: localstorage
    if(Utils.store('HaveACookie') != null){
        var devMode = true;
        if (devMode){
            //Voorloppig niet saven voor development
            applyDefaultAppValues();
        }
        else {
            AppData = Utils.store('HaveACookie');
        }
    } else{
        applyDefaultAppValues();
    }

    updateAll();
    AppData.animation = true;

})();