/* Globale variablen */
var input;
var outputList;
var list = [];
var lists = [];

/* task object*/
var task = function(text){
    this.text = text;
    this.duedate = '';
};

window.onload = function(){

    input = document.getElementById("input");
    outputList = document.getElementById("list");

    /* Even handlers */
    input.addEventListener('change', addTask);
    input.addEventListener('focus', function(){
        this.value = "";
    });
    input.addEventListener('blur', function(){
        this.value = "Add task";
    });
};

function addTask(){
    list.push(new task(input.value));
    input.value = '';
    update();
}

function update(){

    outputList.innerHTML = '';
    var node;

    for(var i in list){

        var del = document.createElement('a');
        del.href = "#";
        del.className = "delete";
        del.id = i;
        node = document.createTextNode("\u2620");
        del.appendChild(node);
        del.addEventListener('click', function(){
            var index = parseInt(this.id);
            list.splice(index, 1);
            update();
        });


        var due = document.createElement('a');
        due.href = "#";
        due.className = "duedate";
        due.id = i;
        node = document.createTextNode(" \u2691");
        due.appendChild(node);
        due.addEventListener('click', function(){
            var index = parseInt(this.id);
            list[index].duedate = "test";
            update();
        });

        var task = document.createElement('li');
        node = document.createTextNode(list[i].text + " - ");
        task.appendChild(node);
        task.appendChild(del);
        task.appendChild(due);
        if (list[i].duedate){
            console.log("duedate set: " + list[i].text);
            var duedate = document.createElement('p');
            node = document.createTextNode("Due: 11/03/15");
            duedate.appendChild(node);
            task.appendChild(duedate);
        }

        outputList.appendChild(task);
    }
}