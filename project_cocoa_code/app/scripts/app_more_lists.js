/* Globale variablen */
var input;
var inputNav;
var outputList;
var nav;
var navList = [];
var activelist;

var delbtn = '\u00D7';

window.onload = function(){

    input = $(".inputTask");
    inputNav = document.getElementById("inputNav");
    outputList = document.getElementById("list");
    nav = document.getElementById("nav");

    inputNav.addEventListener('change', function(){
        navList.push(new List(this.value));
        this.value = "";
        update();
    });
    inputNav.addEventListener('focus', function(){
        this.value = "";
    });
    inputNav.addEventListener('blur', function(){
        this.value = "Add navigation";
    });

    /* add some to navigation */
    navList.push(new List("School"));
    navList.push(new List("Work"));
    navList[0].activated = true;
    activelist = navList[0];
    update();
};

/* task object*/
var task = function(text){
    var thisTask = this;
    var priority = 0;
    this.text = text;
    this.duedate = '';
    this.toHTML = function(){

        var del = document.createElement('a');
        del.className = "delete";
        var node = document.createTextNode(" " + delbtn);
        del.appendChild(node);
        del.addEventListener('click', function(){
            var i = activelist.tasks.indexOf(thisTask);
            if(i != -1) {
                activelist.tasks.splice(i, 1);
            }
            update();
        });

        var prior = document.createElement('span');
        node = document.createTextNode("\u2691 ");
        prior.appendChild(node);
        prior.addEventListener("click", function(){
            priority++;
            update();
        });
        switch (priority){
            case 1:
                prior.className = "yellow";
                break;
            case 2:
                prior.className = "orange";
                break;
            case 3:
                prior.className = "red";
                break;
            default:
                priority = 0;
                prior.className = "";
                break;
        }

        var text = document.createElement('span');
        node = document.createTextNode(this.text);
        text.appendChild(node);
        text.setAttribute("contenteditable","false");
        text.className = "text";
        text.addEventListener("dblclick", function(){
            this.setAttribute("contenteditable","true");
        });
        text.addEventListener("blur", function(){
            this.setAttribute("contenteditable","false");
            thisTask.text = this.innerText;
        });
        text.addEventListener("keypress", function(e){
            if (e.keyCode == 13) {
                this.blur();
                this.setAttribute("contenteditable","false");
                thisTask.text = this.innerText;
            }
        });

        var pencil = document.createElement('a');
        pencil.className = "delete";
        node = document.createTextNode(" \u00B6");
        pencil.appendChild(node);
        pencil.addEventListener('click', function(){
            text.setAttribute("contenteditable","true");
            text.focus();

        });

        var task = document.createElement('li');
        task.appendChild(prior);
        task.appendChild(text);
        task.appendChild(pencil);
        task.appendChild(del);
        return task;
    };
};

/* list object*/
var List = function(name){
    var thisList = this;
    this.name = name;
    this.tasks = [];
    this.activated = false;
    this.activate = function(){
        for (index in navList){
            navList[index].deactivate();
        }
        thisList.activated = true;
        activelist = thisList;
        update();
    };
    this.deactivate = function(){
        this.activated = false;
    };
    this.toHTML = function(){
        var node;

        var del = document.createElement('a');
        del.className = "delete";
        node = document.createTextNode(" " + delbtn);
        del.appendChild(node);
        del.addEventListener('click', function(){
            var i = navList.indexOf(thisList);
            if(i != -1) {
                navList.splice(i, 1);
            }
            update();
        });

        var button = document.createElement("li");
        if (this.activated) {
            node = document.createTextNode("> " + this.name + " " + this.tasks.length);
        }
        else{
            node = document.createTextNode(this.name + " " + this.tasks.length);
        }
        var a = document.createElement("a");
        a.appendChild(node);
        a.addEventListener("click", function(){
            thisList.activate();
            /*for (i in navList){
                navList[i].activated = false;
            }
            thisList.activated = true;
            activelist = thisList;
            update();*/
        });
        button.appendChild(a);
        button.appendChild(del);
        return button;
    }
};

function addTask(){
    if (navList.length > 0) {
        activelist.tasks.push(new task(input.value));
        input.value = '';
        update();
    }
    else{
        alert("Please create a navigation first");
    }
}

function createNav(){
    nav.innerHTML = '';

    if (navList.length > 0){
        if (navList.indexOf(activelist) == -1){
            activelist = navList[0];
            navList[0].activated = true;
        }
    }
    else{
        activelist = [];
    }

    for(var i in navList){
        nav.appendChild(navList[i].toHTML());
    }
}

function createTasks(){
    outputList.innerHTML = '';
    for(var i in activelist.tasks){
        outputList.appendChild(activelist.tasks[i].toHTML());
    }
}

function update(){

    if (navList.length > 0){
        input.disabled = false;
    }
    else{
        input.disabled = true;
    }

    createNav();
    createTasks();

}