/*
Created by: Philippe De Pauw - Waterschoot
Date: 04-11-2014
Name: app.js
Description: JavaScript Routing
*/

(function(){
    //Variables for crossroads and hasher
    var router, hash;

    // Initialize the application
    function init(){
        router = crossroads;//Clone the crossroads object
        hash = hasher;//Clone the hasher object

        //Crossroads settings
        var first = document.querySelector('.page').id;
        var homeRoute = router.addRoute('/',function(){onSectionMatch(first);});
        var sectionRoute = router.addRoute('/{section}');//Add the section route to crossroads
        sectionRoute.matched.add(onSectionMatch);//Hash matches to section route
        //router.routed.add(console.log, console);//Log all crossroads events

        //Hash settings
        hash.initialized.add(onParseHash);//Parse initial hash
        hash.changed.add(onParseHash);//Parse changes in the hash
        hash.init();//Start listening to the hashes
    }

    //Event Listener: listen to the changes in th hash
    function onParseHash(newHash, oldHash){
        //console.log('New: ' + newHash + ' <- Old: ' + oldHash);
        router.parse(newHash);
    }

    //Event Listener: listen to matched routes (parse)
    function onSectionMatch(section){
        AppData.activeList = section;
        var pages = document.querySelectorAll('.page');
        if(pages != null && pages.length > 0){
            _.each(pages,function(page){
               if(page.id == section){
                   page.classList.add('active');
               }else{
                   page.classList.remove('active');
               }
            });
        }


        var items = document.querySelectorAll('.navitem');
        if(items != null && items.length > 0){
            _.each(items,function(item){
                if(item.classList.contains(section)){
                    item.classList.add('active');
                }else{
                    item.classList.remove('active');
                }
            });
        }
    }

    init();//Call the init function
})();