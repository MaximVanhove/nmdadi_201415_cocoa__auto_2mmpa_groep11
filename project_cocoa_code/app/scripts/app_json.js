/* Globale variablen */
var input;
var outputList;
var list = [];
var jsonList;

/* waarde geven aan vars
en event handelers registreren als de pagina geladen is */
window.onload = function(){

    /* waarden voor vars */
    input = document.getElementById("input");
    outputList = document.getElementById("list");

    /* Even handlers */
    input.addEventListener('change', addTask);
    input.addEventListener('focus', function(){
        this.value = "";
    });
    input.addEventListener('blur', function(){
        this.value = "Add task";
    });

    /* to do lijst ophalen uit local storage */
    jsonList = localStorage.todo;
    list = JSON.parse(jsonList);
    if (!list.length > 0){
        list = [];
    }
    update();
};

function addTask(){
    list.push(input.value);
    input.value = '';
    update();
}

function update(){

    /* to-do lijst bewaren in local storage */
    jsonList = JSON.stringify(list);
    localStorage.todo = jsonList;

    outputList.innerHTML = "";
    for(var i in list){
        var node; // local var

        /* aanmaken van verwijder knop <a> */
        var link = document.createElement('a');
        link.href = "#";
        link.className = "delete";
        link.id = i;

        /* tekst toevoegen voor de link */
        node = document.createTextNode("verwijder"); // maakt tekst
        link.appendChild(node);

        /* Even handler die de parent verwijderd (de li)*/
        link.addEventListener('click', function(){
            var index = parseInt(this.id);
            list.splice(index, 1);
            update();
        });

        /* Aanmaken van parent <li> */
        var task = document.createElement('li');
        node = document.createTextNode(list[i] + " - ");
        task.appendChild(node); // Voegt tekst van input toe in li
        task.appendChild(link); // Voegt de link die we maakten toe in li

        outputList.appendChild(task); // Vloegt li toe aan ul (list)
    }
}